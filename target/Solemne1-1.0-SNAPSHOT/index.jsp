<%-- 
    Document   : index
    Created on : 27-sep-2019, 16:38:00
    Author     : PC
--%>

<%@page import="root.services.Dolar"%>
<%@page import="javax.json.JsonArray"%>
<%@page import="java.io.StringReader"%>
<%@page import="javax.json.JsonObject"%>
<%@page import="javax.json.Json"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDate"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <% Client client = ClientBuilder.newClient();
        String apiKey = "3c63675e1c7ab638de6d3f1f1393bc32c20b64d7";

        LocalDate date2 = LocalDate.now();
        String sDate2 = date2.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String fecha[] = sDate2.split("-");

        String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fecha[0] + "/" + fecha[1] + "/dias/" + fecha[2] + "/?formato=JSON&apikey=" + apiKey;

        Response respuestaSBIF = client.target(url).request().get();

        String datoRespuestaSBIF = respuestaSBIF.readEntity(String.class);

        JsonObject j = Json.createReader(new StringReader(datoRespuestaSBIF)).readObject();
        JsonArray valorDolar = j.getJsonArray("Dolares");
        JsonObject jsonExtraido = valorDolar.getJsonObject(0);
        String Fecha = jsonExtraido.getString("Fecha");
        String valor = jsonExtraido.getString("Valor");


    %>
    
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consulta precio Dolar</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        
      
       
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card-header" align="center"><h1>Consulta el precio del dolar</h1></div>
                    <label type="text" class="form-control" align="center" ><h3> Uso del la APK </h3></label>
                    
                   
                    
                   
                       
                    <label type="text" class="form-control"><h5>El precio del dolar el  dia de hoy es: <%=valor%></h5>  </label>
                
                
                    <label type="text" class="form-control" >1.- debe ingresar en lasiguiente direccion  http://DESKTOP-9L2RL1E:8080/Solemne1-1.0-SNAPSHOT/api/dolar/{fecha} </label>
                        <label type="text" class="form-control" >1.- debe ingresar en lasiguiente direccion:  https://dolarask.herokuapp.com/api/dolar/{fecha} (pero no funciona)</label>
                    <label type="text" class="form-control" >2.- Debe ingresar la fecha en la url en el lugar que dice {fecha} de la siguiente manera:yyyy-mm-dd donde:
                        , mm corresponde al valor del mes y dd corresponde al valor del dia</label>

                    <label type="text" class="form-control" >---a.- yyyy corresponde al valor del año</label>
                    <label type="text" class="form-control" >---b.- mm corresponde al valor del mes</label>
                    <label type="text" class="form-control" >---c.- dd corresponde al valor del día</label>
                    <label type="text" class="form-control" >---d.- yyy-mm-dd deben ir separados por un "-" (solo el guíon)</label>
                    <label type="text" class="form-control" >3.- Si la fecha que ingresa coincide con un dia feriado o fin de semana,
                        se le devolvra el valor del dolar del ultimo dia hábil</label>

                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>

